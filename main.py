import logging
import func.twitter
from func.config import CONFIG

# Log to both stdout and file
logging.basicConfig(filename=CONFIG["LOG_PATH"], level=logging.INFO)
logging.getLogger().addHandler(logging.StreamHandler())

logging.info("RUNNING ENVIRONMENT: {env}".format(env=CONFIG["ENV"]))

func.twitter.initPostedData()

func.twitter.authenticate(func.twitter.startPostLoop)
