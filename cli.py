# NOTE 22/07/2023
# Currently broken due to changes to the way posts are validated

import argparse
from re import I
import time
import logging

from func import twitter, api
from func.config import CONFIG


def str_to_bool(text) -> bool:
    return text in ["True", "true"]


def main():
    parser_top = argparse.ArgumentParser(
        description="CLI for Szurubot", prog="Szurubot CLI"
    )

    parser = parser_top.add_mutually_exclusive_group(required=False)

    parser.add_argument(
        "--status", action="store_true", help="Show the status of the bot"
    )
    parser.add_argument(
        "--set-index",
        metavar="index",
        action="store",
        type=int,
        help="Set the posting index",
    )
    parser.add_argument(
        "--post",
        metavar=("id", "is_dry_run", "ignore_interval", "check_valid"),
        nargs=4,
        action="store",
        help="Tweet a post given its ID",
    )
    parser.add_argument(
        "--update-cache",
        action="store_true",
        help="Clear the cache and get Szurubooru post data",
    )

    command = parser_top.parse_args()

    try:
        if command.status:
            data = twitter.getPostedData()
            if data.get("last_post_time"):
                print(
                    "Current index: {index}. Last post: {last_id}. Next post time is at {time}.".format(
                        index=data["index"],
                        last_id=data["posts"][-1] if data["posts"] else "None",
                        time=time.strftime(
                            "%D %H:%M:%S",
                            time.localtime(
                                data["last_post_time"] + CONFIG["POST_INTERVAL"]
                            ),
                        ),
                    )
                )
            else:
                print("Current index: {index}.".format(index=data["index"]))
        elif command.set_index:
            twitter.setIndex(command.set_index)
            print("Index set to " + str(command.set_index))
        elif command.post:
            all_valid_posts = api.get_all_valid_posts()
            posted_data = twitter.getPostedData()

            id = int(command.post[0])

            dry_run = False
            ignore_interval = False
            check_valid = False
            if len(command.post) == 2:
                dry_run = str_to_bool(command.post[1])
            elif len(command.post) == 3:
                dry_run = str_to_bool(command.post[1])
                ignore_interval = str_to_bool(command.post[2])
            elif len(command.post) == 4:
                dry_run = str_to_bool(command.post[1])
                ignore_interval = str_to_bool(command.post[2])
                check_valid = str_to_bool(command.post[3])

            if id <= 0:
                print("Invalid ID")
                return
            elif id > all_valid_posts[0]:
                print(
                    "ID out of range. The most recent post's ID is "
                    + str(all_valid_posts[0])
                )
                return
            elif id in posted_data["posts"] and not dry_run:
                print("Post {id} is already Tweeted.".format(id=id))
                return
            elif check_valid:
                if not twitter.checkForValid(id, all_valid_posts):
                    print("Post {id} is not a valid post.".format(id=id))
                    return

            if twitter.hasPostIntervalPassed(posted_data) or ignore_interval:
                is_successfully_tweeted = twitter.post(id, dry_run)
                if is_successfully_tweeted:
                    print(
                        "{dry_run}Tweeted post {id}.".format(
                            dry_run="(DRY RUN) " if dry_run else "", id=id
                        )
                    )
                else:
                    print(
                        "{dry_run}Failed to Tweet post {id}.".format(
                            dry_run="(DRY RUN) " if dry_run else "", id=id
                        )
                    )
            else:
                print(
                    "Not enough time has passed since the last post at "
                    + time.strftime(
                        "%D %H:%M", time.localtime(posted_data["last_post_time"])
                    )
                )
                print(
                    "the next post time is at "
                    + time.strftime(
                        "%D %H:%M",
                        time.localtime(
                            posted_data["last_post_time"] + CONFIG["POST_INTERVAL"]
                        ),
                    )
                )
        elif command.update_cache:
            data = api.update_post_data()
            if data:
                print("Cache updated")
            else:
                print("Failed to update cache")
    except Exception as e:
        print(e)


# Ensure that logging works
logging.basicConfig(filename=CONFIG["LOG_PATH"], level=logging.INFO)
logging.getLogger().addHandler(logging.StreamHandler())
logging.info("RUNNING ENVIRONMENT: {env}".format(env=CONFIG["ENV"]))

twitter.initPostedData()

twitter.authenticate(main)
