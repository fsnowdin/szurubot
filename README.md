# Szurubot

This is a bot that looks up posts from a [Szurubooru](https://github.com/rr-/szurubooru) instance and post them at regular intervals to Twitter.

This bot uses Python 3.

## Example bots
- [Yumebot](https://twitter.com/yumebooru)
- [RPGM Art Bot](https://twitter.com/rpgmbot)

## Usage

**1. Download to your system**

```
git clone https://gitlab.com/fsnowdin/szurubot.git

```

**2. Install dependencies**

- Make sure you're in the project folder
```
cd szurubot
```

- Install
```
pip3 install -r requirements.txt
```

- If installing with `requirements.txt` doesn't work, manually install required packages with:
```
pip3 install toml flask waitress tweepy
```

**3. Configure** 
- Copy the example config file
```
cp ./config/example.toml ./config/config.toml
```

- Configure to your usage
    - Add in info about your Szurubooru instance and your account credentials.
    - Add in your Twitter API keys and access tokens (leave access tokens blank for OAuth authetication)
    - Change how you want the bot to filter the posts, the format of the Tweets, intervals between Tweets, etc.
    - Add in your [YOURLS](https://yourls.org/) server credentials if you're using it as a URL shortener (Twitter links on Tweets does not look pretty with this bot)
```
edit ./config/config.toml
```

**4. Run**

```
python3 ./main.py
```

**5. Miscellaneous**

- Logs are stored at `log/app.log`. 
- Posts that have been Tweeted have their IDs stored at `data/posted.json`.
- If you use OAuth authentication for Twitter, your access tokens are stored at `data/tokens.json`.

## License
[GPLv3](/LICENSE)
